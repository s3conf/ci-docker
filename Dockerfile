FROM docker:stable

RUN apk add --update python3 \
    && pip3 install -U docker-compose s3conf==0.8.4 \
    && rm -rf /tmp/* /var/cache/apk/*

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh"]
